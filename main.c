#include <stdio.h>
#include <stdlib.h>

//author: Geanina Tambaliuc

typedef struct account
{
    long int accNo; //account number
    int pin;
    long int ssn;
    float accAmount;

}Accounts;

int anotherTrans, systemN, n; //systemN is the number of accounts in the system
Accounts accounts[30];
//check if an account is in the system

int checkAccountExistence(long int accountTransfer)
{
    for(int i=0;i<=systemN;i++)
        if(accounts[i].accNo==accountTransfer) return i;
    return -1;
}

//check the pin of an account, returns 1 if the pin is correct, 0 otherwise
int checkPin(int pin, long int accNo)
{
    //check if the account exists in the system and memorize the position
    int position=checkAccountExistence(accNo);
    if(position==-1)
    {
        printf("The account is not in our system! \n");
    }
    else
    {
        if(accounts[position].pin==pin) return 1;
    }

    return 0;
}

//function for opening an account
void openAccount()
{
    if(n<=systemN)
    {
        printf("You reached the limit! You can only create 30 accounts. \n");
    }
    else{
        systemN=systemN+1; //a new account added to the system

        long int accNo; //account number
        int pin;
        long int ssn;
        float accAmount;

        printf("Please enter the SSN: ");
        scanf("%d",&ssn);

        //clear the buffer
        while ((getchar()) != '\n');


        if(ssn>99999999 && ssn<=999999999)
        {accounts[systemN].ssn=ssn;}
        else
        {
            int ok=0;
            while(ok!=1)
            {
                printf("Your SSN is not valid. It should contain only digits and be 9-digits long. Please enter it again: ");
                scanf("%d",&ssn);
                //clear the buffer
                while ((getchar()) != '\n');

                if(ssn>99999999 && ssn<=999999999)
                {
                    accounts[systemN].ssn=ssn;
                    ok=1;
                }
                else ok=0;
            }
        }

        printf("\n\n");
        printf("Please enter the PIN number: ");
        scanf("%d",&pin);
        //clear the buffer
        while ((getchar()) != '\n');


        if(pin>999 && pin<=9999)
        {accounts[systemN].pin=pin;}
        else
        {
            int ok=0;
            while(ok!=1)
            {
                printf("Your PIN is not valid. It should contain only digits and be 4-digits long. Please enter it again: ");
                scanf("%d",&pin);
                //clear the buffer
                while ((getchar()) != '\n');


                if(pin>999 && pin<=9999)
                {
                    accounts[systemN].pin=pin;
                    ok=1;
                }
                else ok=0;
            }
        }

        printf("\n\n");
        printf("Please enter the Account number: ");
        scanf("%d",&accNo);
        //clear the buffer
        while ((getchar()) != '\n');


        if(accNo>9999 && accNo<=99999)
        {accounts[systemN].accNo=accNo;}
        else
        {
            int ok=0;
            while(ok!=1)
            {
                printf("Your Account Number is not valid. It should contain only digits and be 5-digits long.Keep in mind - it cannot start with a 0!! Please enter it again: ");
                scanf("%d",&accNo);
                //clear the buffer
                while ((getchar()) != '\n');

                if(accNo>9999 && accNo<=99999)
                {
                    accounts[systemN].accNo=accNo;
                    ok=1;
                }
                else ok=0;
            }
        }

        accounts[systemN].accAmount=0; // the initial balance is 0;

        printf("\n\n");
        printf("Account created successful! Account No: %d", accounts[systemN].accNo);



    }
}

void closeAccount(long int accountNo)
{
    //check if the account exists in the system and memorize the position
    int position=checkAccountExistence(accountNo);
    if(position==-1)
    {
        printf("The account is not in our system! \n");
    }
    else
    {
        for(int i=position;i<n;i++)
            accounts[i]=accounts[i+1];
        systemN--;

        printf("Account closed successful!");
    }

    return 0;

}

//Main function for transactions
//acc is the customer account
//accounts is the array containing all the accounts
void transaction(Accounts acc, int position)
{
    int choice; //used to store the customer's choice
    printf("\nWhat would you like to do today? \n\n");
    printf("1.Deposit funds \n");
    printf("2.Withdraw funds \n");
    printf("3.Query account \n");
    printf("4.Transfer Funds \n");
    printf("5.Cancel \n");
    printf("Please enter the number of the transaction that you want to execute: ");
    scanf("%d", &choice);
    //clear the buffer
    while ((getchar()) != '\n');


    //Deposit funds
    if(choice==1){
        printf("Please enter the amount you want to deposit: ");
        float amount; //the amount to deposit
        scanf("%f", &amount);
        //clear the buffer
        while ((getchar()) != '\n');
        acc.accAmount=acc.accAmount + amount;
        accounts[position]=acc;
        printf("Thank you! Your new balance is: %f", acc.accAmount);
        printf("\n\n");

        //ask for another transaction
        printf("Do you want to execute another transaction? Press 1 for YES or 0 for NO. \n");
        scanf("%d", &anotherTrans);
        //clear the buffer
        while ((getchar()) != '\n');
        if(anotherTrans==1)
        {
            //execute another transaction
            transaction(acc,position);
        }
        else if(anotherTrans==0)
        {
            choice=5;
        }
        else
        {
            printf("Your input is not valid. Going back to main menu!\n");
            choice=5;
        }
    }
    //Withdraw funds
    else if(choice==2){
        printf("Please enter the amount you want to withdraw: ");
        float amountWithdraw;
        scanf("%f",&amountWithdraw);
        //clear the buffer
        while ((getchar()) != '\n');

        //check if there are insufficient funds
        if(amountWithdraw > acc.accAmount)
        {
            printf("INSUFFICIENT FUNDS!");
            //ask for another transaction
            printf("Do you want to execute another transaction? Press 1 for YES or 0 for NO. \n");
            scanf("%d", &anotherTrans);
            //clear the buffer
            while ((getchar()) != '\n');

            if(anotherTrans==1)
            {
                //execute another transaction
                transaction(acc, position);
            }
            else if(anotherTrans==0)
            {
                choice=5;
            }
            else
            {
                printf("Your input is not valid. Going back to main menu!\n");
                choice=5;
            }
        }
        else
        {
            //withdraw the amount chosen

            acc.accAmount=acc.accAmount - amountWithdraw;
            accounts[position]=acc;
            printf("Withdrawn successful!Your current balance is: %f ",acc.accAmount);
            printf("\n");

            //ask for another transaction
            printf("Do you want to execute another transaction? Press 1 for YES or 0 for NO. \n");
            scanf("%d", &anotherTrans);
            //clear the buffer
            while ((getchar()) != '\n');

            if(anotherTrans==1)
            {
                //execute another transaction
                transaction(acc, position);
            }
            else if(anotherTrans==0)
            {
                choice=5;
            }
            else
            {
                printf("Your input is not valid. Going back to main menu!\n");
                choice=5;
            }
        }
    }

    //Query account
    else if(choice==3){
        printf("Your current balance is: %f", acc.accAmount);
        printf("\n");

         //ask for another transaction
            printf("Do you want to execute another transaction? Press 1 for YES or 0 for NO. \n");
            scanf("%d", &anotherTrans);
            //clear the buffer
            while ((getchar()) != '\n');

            if(anotherTrans==1)
            {
                //execute another transaction
                transaction(acc,  position);
            }
            else if(anotherTrans==0)
            {
                choice=5;
            }
            else
            {
                printf("Your input is not valid. Going back to main menu!\n");
                choice=5;
            }
    }
    // Transfer funds
    else if(choice==4){

        printf("Please enter the amount you want to transfer: ");
        float transferAmount;
        scanf("%f", &transferAmount);
        //clear the buffer
        while ((getchar()) != '\n');

        if(transferAmount>acc.accAmount)
        {
            printf("INSUFFICIENT FUNDS!");
            //ask for another transaction
            printf("Do you want to execute another transaction? Press 1 for YES or 0 for NO. \n");
            scanf("%d", &anotherTrans);
            //clear the buffer
            while ((getchar()) != '\n');

            if(anotherTrans==1)
            {
                //execute another transaction
                transaction(acc, position);
            }
            else if(anotherTrans==0)
            {
                choice=5;
            }
            else
            {
                printf("Your input is not valid. Going back to main menu!\n");
                choice=5;
            }

        }
        else
        {

            printf("Please enter the account number of the receiver: ");
            long int accountTransfer;
            scanf("%d", &accountTransfer);
            //clear the buffer
            while ((getchar()) != '\n');


            //check if the account exists
            int check = checkAccountExistence(accountTransfer);

            if(check==-1)
            {
                printf("The account number you entered is not in our system!");

                //ask for another transaction
                printf("Do you want to execute another transaction? Press 1 for YES or 0 for NO. \n");
                scanf("%d", &anotherTrans);
                //clear the buffer
                while ((getchar()) != '\n');

                if(anotherTrans==1)
                {
                    //execute another transaction
                    transaction(acc, position);
                }
                else if(anotherTrans==0)
                {
                    choice=5;
                }
                else
                {
                    printf("Your input is not valid. Going back to main menu!\n");
                   choice=5;
                }
            }
            else
            {
                acc.accAmount=acc.accAmount-transferAmount;
                accounts[position]=acc;
                accounts[check].accAmount=accounts[check].accAmount+transferAmount;

                printf("Transfer successful! Your current balance is: %f", acc.accAmount);
                printf("\n\n");

                int trans=0;
                //ask for another transaction
                printf("Do you want to execute another transaction? Press 1 for YES or 0 for NO. \n");
                scanf("%d", &trans);
                //clear the buffer
                while ((getchar()) != '\n');

                if(trans==1)
                {
                    //execute another transaction
                    transaction(acc, position);
                }
                else if(trans==0)
                {
                    choice=5;
                }
                else
                {
                    printf("Your input is not valid. Going back to main menu!\n");
                    choice=5;
                }

            }



        }}
    else if(choice==5){
        printf("Transaction CANCELED!");}


    else
        {printf("Your choice is invalid! Your input can only be 1,2,3,4, or 5.");}



}
void displayMenu()
{
    printf("\nFor testing purposes the following accounts have been created: accNo: 12345 pin:1234, accNo:12344 pin:1233 \n");
    printf("What do you want to do?\n");
    printf("Please enter a number: \n");
    printf("1.Bank Staff Functions \n");
    printf("2.Customer Functions \n");
    printf("3.Exit \n");
}


int main()
{

    n=30;//the system can hold only 30 accounts
    systemN=0;

    //accounts created for testing purposes
    accounts[0].ssn=123456789;
    accounts[0].accNo=12345;
    accounts[0].pin=1234;
    accounts[0].accAmount=0;
    systemN=1;
    accounts[1].ssn=123456788;
    accounts[1].accNo=12344;
    accounts[1].pin=1233;
    accounts[1].accAmount=0;
    systemN=2;

    int choice;
    do
    {

        displayMenu();
        scanf("%d",&choice);
        //clear the buffer
        while ((getchar()) != '\n');

        if(choice==1)
        {
            int secondChoice;
            do
            {
                printf("\nWhat do you want to do?\n");
                printf("0.Back to main menu \n");
                printf("1.Open Account \n");
                printf("2.Close Account \n");
                printf("3.Exit \n");
                printf("Please enter the number of the option you want: ");
                scanf("%d",&secondChoice);
                //clear the buffer
                while ((getchar()) != '\n');



                if(secondChoice==1)
                    openAccount();
                else if(secondChoice==2)
                {
                    long int accountToDelete;
                    printf("\n\n Please enter the account you want to close: ");
                    scanf("%d",&accountToDelete);
                    //clear the buffer
                    while ((getchar()) != '\n');


                    closeAccount(accountToDelete);
                }
                else if(secondChoice==3)
                    choice=3;
            }while(secondChoice!=3 && secondChoice!=0);
        }
        else if(choice==2)
        {

            long int accountNo;
            int pin;
            int exists;
            int ok=0;
            do
            {
                printf("\n Please enter your account number: ");
                scanf("%d",&accountNo);
                //clear the buffer
                while ((getchar()) != '\n');


                printf("\n Please enter your pin: ");
                scanf("%d",&pin);
                //clear the buffer
                while ((getchar()) != '\n');


                exists=checkPin(pin,accountNo);

                if(exists==0)
                {
                    ok++;
                    if(ok==3)
                    {
                        printf("You entered a wrong pin 3 times, the system will terminate the transaction.\n");
                        choice=3;
                    }
                    else
                    {
                        printf("\n Information not valid! Try again...");

                    }



                }
                else if(exists==1)
                {
                    int position=checkAccountExistence(accountNo);
                    transaction(accounts[position],position);

                }
            }while(exists!=1 && ok!=3);

        }
    }while(choice!=3);


    return 0;
}
